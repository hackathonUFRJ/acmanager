#include <IRremote.h>
#include <IRremoteInt.h>


//Signals
const unsigned short maxSize = 228; // 800;
unsigned short onSize = maxSize - 1;
unsigned short offSize = maxSize - 1;
//unsigned int onSignal[maxSize - 1] = {4560, 4408, 642, 1566, 640, 467, 636, 1568, 635, 1574, 638, 466, 640, 464, 635, 1572, 644, 462, 637, 465, 644, 1565, 634, 470, 644, 458, 634, 1574, 640, 1568, 643, 461, 634, 1574, 643, 460, 639, 1016, 638, 1019, 640, 1568, 642, 1567, 640, 1016, 637, 1571, 642, 1566, 648, 1561, 648, 1008, 642, 1014, 643, 460, 635, 470, 644, 1011, 641, 463, 640, 463, 639, 1018, 645, 1563, 638, 1016, 642, 462, 638, 1018, 641, 463, 646, 457, 643, 461, 638, 1018, 643, 459, 636, 1021, 638, 1570, 640, 1015, 641, 1567, 646, 1562, 649, 1560, 644, 5245, 4589, 4404, 640, 1568, 636, 468, 642, 1566, 642, 1568, 640, 463, 646, 458, 638, 1570, 646, 458, 641, 462, 639, 1570, 640, 462, 649, 455, 636, 1573, 638, 1570, 642, 463, 638, 1569, 642, 463, 640, 1013, 638, 1020, 641, 1567, 638, 1571, 641, 1014, 647, 1561, 645, 1564, 642, 1566, 641, 1014, 639, 1018, 638, 466, 641, 462, 639, 1017, 642, 462, 639, 464, 642, 1014, 644, 1563, 640, 1017, 636, 468, 637, 1018, 644, 463, 637, 464, 642, 463, 638, 1018, 642, 461, 643, 1012, 639, 1569, 646, 1012, 639, 1568, 635, 1573, 644, 1565, 638};
//unsigned int offSignal[maxSize - 1] = {4557, 4411, 644, 1564, 647, 479, 614, 1572, 636, 1572, 638, 485, 614, 489, 620, 1568, 642, 484, 617, 485, 618, 1571, 638, 482, 626, 479, 623, 1567, 645, 1562, 635, 489, 617, 1571, 640, 484, 623, 1022, 634, 1022, 633, 1566, 647, 1561, 642, 1026, 632, 1564, 639, 1570, 641, 1566, 642, 1022, 634, 1025, 628, 478, 626, 484, 611, 1032, 628, 482, 625, 484, 625, 1019, 634, 1566, 643, 1022, 636, 478, 623, 1024, 631, 482, 620, 483, 623, 480, 617, 1028, 626, 483, 624, 1024, 631, 1572, 642, 1023, 626, 1572, 642, 1566, 642, 1565, 641, 5248, 4581, 4414, 640, 1570, 642, 479, 614, 1577, 634, 1574, 639, 479, 624, 481, 628, 1565, 644, 480, 618, 483, 624, 1569, 634, 484, 630, 475, 623, 1569, 636, 1572, 642, 476, 625, 1568, 638, 478, 628, 1025, 634, 1021, 629, 1572, 636, 1571, 638, 1026, 626, 1575, 636, 1572, 634, 1575, 635, 1028, 634, 1018, 633, 484, 621, 482, 620, 1030, 634, 470, 626, 478, 638, 1018, 634, 1566, 643, 1016, 638, 469, 640, 1019, 629, 476, 634, 468, 626, 478, 633, 1022, 629, 482, 622, 1025, 636, 1565, 638, 1025, 637, 1563, 640, 1568, 637, 1572, 633};

unsigned int onSignal[maxSize-1] = {3684, 1612, 600, 1056, 580, 1080, 576, 476, 580, 484, 604, 452, 600, 1056, 604, 452, 572, 492, 572, 1084, 604, 1056, 580, 476, 600, 1068, 576, 476, 604, 448, 600, 1056, 580, 1096, 600, 452, 572, 1084, 604, 1056, 576, 488, 576, 472, 608, 1056, 576, 476, 600, 460, 580, 1080, 576, 476, 604, 452, 572, 492, 572, 480, 576, 476, 576, 476, 580, 484, 580, 476, 600, 452, 572, 476, 580, 484, 604, 452, 600, 452, 576, 480, 572, 488, 604, 448, 580, 472, 580, 1080, 580, 484, 580, 472, 576, 1084, 576, 472, 580, 484, 580, 476, 600, 452, 580, 472, 576, 1100, 576, 476, 604, 452, 600, 448, 580, 484, 580, 1080, 576, 1084, 576, 1084, 572, 492, 576, 476, 600, 452, 604, 452, 600, 464, 576, 476, 604, 448, 576, 476, 580, 1092, 576, 476, 580, 476, 572, 476, 580, 492, 600, 448, 576, 476, 580, 476, 572, 492, 576, 480, 572, 476, 580, 472, 580, 488, 576, 476, 576, 480, 600, 448, 580, 488, 600, 452, 572, 480, 576, 476, 576, 488, 580, 476, 600, 452, 580, 476, 572, 492, 572, 476, 580, 476, 576, 476, 580, 488, 576, 476, 572, 484, 572, 476, 576, 488, 580, 476, 600, 452, 572, 484, 572, 492, 572, 476, 580, 476, 576, 476, 572, 492, 576, 1084, 576, 476, 572, 1088, 572, 484, 544};
unsigned int offSignal[maxSize-1] = {3640, 1656, 576, 1084, 572, 1084, 576, 480, 572, 492, 572, 476, 576, 1084, 576, 480, 572, 492, 572, 1084, 576, 1084, 576, 476, 572, 1100, 572, 480, 572, 480, 572, 1084, 576, 1100, 568, 484, 572, 1080, 580, 1080, 576, 488, 576, 480, 572, 1084, 576, 480, 572, 492, 568, 1092, 568, 484, 572, 476, 576, 488, 576, 480, 576, 476, 572, 480, 576, 488, 576, 476, 576, 480, 572, 476, 576, 488, 576, 480, 576, 476, 572, 484, 572, 488, 576, 476, 576, 480, 572, 476, 576, 488, 576, 480, 572, 1084, 576, 480, 568, 496, 568, 480, 576, 480, 572, 480, 572, 1100, 576, 480, 568, 484, 572, 480, 572, 492, 576, 1080, 576, 1088, 568, 1088, 572, 492, 576, 476, 576, 480, 568, 484, 572, 496, 568, 480, 576, 480, 572, 480, 568, 1104, 572, 480, 572, 476, 576, 480, 572, 492, 576, 480, 568, 480, 576, 480, 572, 492, 576, 480, 568, 480, 576, 476, 576, 492, 572, 480, 572, 484, 568, 480, 576, 488, 576, 480, 568, 484, 572, 480, 572, 492, 576, 480, 568, 484, 572, 476, 576, 492, 572, 480, 576, 480, 568, 480, 576, 488, 576, 480, 572, 480, 572, 484, 568, 496, 572, 476, 576, 480, 568, 484, 572, 496, 568, 480, 576, 476, 576, 1080, 576, 1096, 576, 480, 572, 480, 568, 1088, 572, 488, 540};

//Recepção
volatile int irBuffer[maxSize]; //stores timings - volatile because changed by ISR
volatile unsigned short x = 0; //Pointer thru irBuffer - volatile because changed by ISR
unsigned int maxError = 100;
int state = 0;

//Transmissão
IRsend irsend;

//#######################IMPORTANTE#######################
//Porta de envio padrao de sinal: PWM 9(MEGA) e 3(PROMINI)

void setup()
{
  Serial.begin(9600);
  pinMode(3, OUTPUT);
  //attachInterrupt(digitalPinToInterrupt(2), rxIR_Interrupt_Handler, CHANGE);//set up ISR for receiving IR signal
}

void loop()
{
  //Transmissão
  if (Serial.available() > 0)
  {
    state = Serial.read();
    switch(state)
    {
      case 'l':
        ligarAr();
      break;
      case 'c':
        carregar();
      break;
      case 'd':
        desligarAr();
    }
  }

  //Recepção
  attachInterrupt(digitalPinToInterrupt(2), rxIR_Interrupt_Handler, CHANGE);//re-enable ISR for receiving IR signal
  //Serial.println(F("Press the button on the remote now - once only"));
  delay(3000); // pause 3 secs
  if (x)
  { //if a signal is captured
    //Serial.println();
    //Serial.print(F("Raw: (")); //dump raw header format - for library
    //Serial.print((x - 1));
    //Serial.print(F(") "));
    detachInterrupt(digitalPinToInterrupt(2));//stop interrupts & capture until finished here
    int aux = 0;
    for (int i = 1; i < x; i++)
    { //now dump the times
      //if (!(i & 0x1)) Serial.print(F("-"));
      //Serial.print(irBuffer[i] - irBuffer[i - 1 - aux]);
      if(irBuffer[i] - irBuffer[i - 1 - aux] < maxError)
        aux++;
      irBuffer[i] = irBuffer[i + aux];
      //Serial.print(F(", "));
    }
    x = 0;

    //Serial.println();
    bool rec = 1;
    for (int i = 0; i < onSize; i++)
    {
      //Serial.print(max(irBuffer[i + 1] - irBuffer[i], onSignal[i]) - min(irBuffer[i + 1] - irBuffer[i], onSignal[i]));
      //Serial.print(F(", "));
      if(max(irBuffer[i + 1] - irBuffer[i], onSignal[i]) - min(irBuffer[i + 1] - irBuffer[i], onSignal[i]) >= maxError)
      {
        //Serial.println(F("Errou aqui: "));
        //Serial.print(i);
        //Serial.print(F(", "));
        //Serial.print(irBuffer[i + 1] - irBuffer[i]);
        rec = 0;
        break;
      }
    }
    if(rec)
    {
      //Serial.println();
      Serial.print(F("l"));
    }

    
    rec = 1;
    for (int i = 0; i < onSize; i++)
    {
      if(max(irBuffer[i + 1] - irBuffer[i], offSignal[i]) - min(irBuffer[i + 1] - irBuffer[i], offSignal[i]) >= maxError)
      {
        rec = 0;
        break;
      }
    }
    if(rec)
      Serial.print(F("d"));
    
    //Serial.println();
    //Serial.println();
  }
}

void carregar()
{
  //Recebe os códigos novos para ON e OFF, enviados pelo computador
  onSize = Serial.read();
  maxError = onSignal[0];
  for(int i = 1; i < onSize; i++)
  {
    onSignal[i] = Serial.read();
    if(onSignal[i] > maxError)
      maxError = onSignal[i];
  }
  offSize = Serial.read();
  for(int i = 0; i < offSize; i++)
  {
    offSignal[i] = Serial.read();
    if(offSignal[i] > maxError)
      maxError = offSignal[i];
  }
  maxError = maxError / 2;
}

void desligarAr()
{
  //Serial.println("Desligando...");
  irsend.sendRaw(offSignal, offSize, 38);
  //Serial.println("Executando a 38kHz");
}

void ligarAr()
{
  //Serial.println("Ligando...");
  irsend.sendRaw(onSignal, onSize, 38);
  //Serial.println("Executando a 38kHz");
}

void rxIR_Interrupt_Handler()
{
  if (x > maxSize) return; //ignore if irBuffer is already full

  irBuffer[x++] = micros(); //just continually record the time-stamp of signal transitions
}
