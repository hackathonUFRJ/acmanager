#include "stdafx.h"
#include "RoomsC.h"


CRoom* CRoomsC::getRoomByName(CString name)
{
	for (vector<CRoom*>::iterator it = m_pRooms.begin(); it != m_pRooms.end(); it++) {
		if ((*it)->m_szName == name)
			return (*it);
	}
	return nullptr;
}

void CRoomsC::add(CRoom* room)
{
	m_pRooms.push_back(room);
}

CRoomsC::CRoomsC()
{

}


CRoomsC::~CRoomsC()
{
	for (vector<CRoom*>::iterator it = m_pRooms.begin(); it != m_pRooms.end(); it++)
		delete (*it);
	m_pRooms.clear();
}
