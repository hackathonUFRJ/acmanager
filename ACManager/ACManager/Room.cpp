#include "stdafx.h"
#include "Room.h"
#include <iostream>
#include <fstream>
#include <sstream>

#using <System.dll>

#define BAUD_RATE 9600

#define LOG_FILE_DEST "C:\\Users\\Pedro\\Desktop\\Hackaton\\ACManager\\Debug\\Log.txt"

using namespace std;

using namespace System;

using namespace System::IO::Ports;

string CRoom::bluetooth_port = "None";

void CRoom::setActive(bool state)
{
	m_isActive = state;
	if (state)
		m_TimeLeftNow = m_TimeDuration;
	else
		m_TimeLeftNow = CTimeSpan(0,0,0,0);

	ofstream myfile;
	myfile.open(LOG_FILE_DEST, ofstream::out | ofstream::app);
	char str[80];
	if (state)
		myfile << "["<< m_szName + "] was turned ON in ";
	else
		myfile << "["<< m_szName + "] was turned OFF in ";
	time_t result = time(NULL);
	ctime_s(str, sizeof(str), &result);
	myfile << string(str);
	myfile.close();
	
	std::wstringstream wsstream; 
	wsstream << CRoom::bluetooth_port.c_str();
	String^ wbluetooth_port = gcnew String(wsstream.str().c_str());


	// arduino settings
	SerialPort^ arduino;
	arduino = gcnew SerialPort(wbluetooth_port, BAUD_RATE);

	CString temp;
	// open port
	try
	{
		arduino->Open();
		if (state)
			arduino->WriteLine("l");
		else
			arduino->WriteLine("d");

		// close port to arduino
		arduino->Close();
	}
	catch (IO::IOException^ e)
	{
		temp.Format(L"%S: Port %S is not ready", e->GetType()->Name, wbluetooth_port);
		MessageBox(GetActiveWindow(), temp, L"Exception!", MB_OK);
	}
	catch (ArgumentException^ e)
	{
		temp.Format(L"%S: incorrect port name syntax, must start with COM/com", e->GetType()->Name);
		MessageBox(GetActiveWindow(), temp, L"Exception!", MB_OK);
	}

}

CRoom::CRoom(CString name, CTimeSpan duration)
{
	m_szName = name;
	m_TimeDuration = duration;
	m_TimeLeftNow = 0;
	m_isActive = false;
}

CRoom::CRoom()
{

}


CRoom::~CRoom()
{
}
