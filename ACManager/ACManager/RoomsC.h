#pragma once
#include "Room.h"

using namespace std;

class CRoomsC
{

public:
	vector<CRoom*> m_pRooms;


	CRoom* getRoomByName(CString);
	void add(CRoom*);

	CRoomsC();
	~CRoomsC();
};

