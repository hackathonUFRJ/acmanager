#pragma once
#include<string>
#include<vector>

using namespace std;

class CRoom
{

public:
	static string bluetooth_port;
	bool m_isActive;
	CString m_szName;
	CTimeSpan m_TimeDuration;
	CTimeSpan m_TimeLeftNow;

	void setActive(bool);

	CRoom(CString, CTimeSpan);
	CRoom();
	~CRoom();
};


