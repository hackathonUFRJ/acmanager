
// ACManagerDlg.cpp : implementation file
//

#include "stdafx.h"
#include <regex>
#include "ACManager.h"
#include "ACManagerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define ROOMS_FILE_DEST "C:\\Users\\Pedro\\Desktop\\Hackaton\\ACManager\\Debug\\Rooms.txt"
#define RECEIVE_EDIT	WM_USER+1

using namespace std;

// CEditRoomDlg dialog used for App About

class CEditRoomDlg : public CDialogEx
{
public:
	CEditRoomDlg();
	CString m_roomName;
	COleDateTime m_cdtime;
	CDateTimeCtrl m_TimeDurationCtrl;
// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

	protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnDtnDatetimechangeDatetimepicker1(NMHDR *pNMHDR, LRESULT *pResult);
};

CEditRoomDlg::CEditRoomDlg() : CDialogEx(IDD_DIALOG1)
{

}

void CEditRoomDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DATETIMEPICKER1, m_TimeDurationCtrl);
}

BEGIN_MESSAGE_MAP(CEditRoomDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CEditRoomDlg::OnBnClickedOk)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DATETIMEPICKER1, &CEditRoomDlg::OnDtnDatetimechangeDatetimepicker1)
END_MESSAGE_MAP()


// CACManagerDlg dialog

BOOL CEditRoomDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	m_TimeDurationCtrl.SetTime(m_cdtime);
	SetWindowText(CString("Edit ")+m_roomName+CString(" settings"));
	return TRUE;
}



CACManagerDlg::CACManagerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_ACMANAGER_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CACManagerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_ListCtrl);
}

BEGIN_MESSAGE_MAP(CACManagerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST1, &CACManagerDlg::OnLvnItemchangedList1)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST1, &CACManagerDlg::OnNMDblclkList1)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST1, &CACManagerDlg::OnNMCustomdrawList1)
	ON_WM_TIMER()
	ON_MESSAGE(RECEIVE_EDIT, &CACManagerDlg::OnReceiveEdit)
END_MESSAGE_MAP()


// CACManagerDlg message handlers

BOOL CACManagerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	


	FILE *pFile;
	fopen_s(&pFile, ROOMS_FILE_DEST, "r");
	if (pFile == nullptr) {
		MessageBox(L"Could not find Rooms.txt.", L"Exception!", MB_OK);
		abort();
	}
	char line[256];
	int lineIndex = 0;
	while (fgets(line, sizeof(line), pFile)) {
		if (lineIndex == 0) {
			CRoom::bluetooth_port = string(line);
			CRoom::bluetooth_port.erase(remove(CRoom::bluetooth_port.begin(), CRoom::bluetooth_port.end(), '\n'), CRoom::bluetooth_port.end());
		}
		smatch sm;
		std::string strStd(line);
		if (regex_match(strStd, sm, std::regex("ROOM\\s*=\\s*(.*),\\s*TIME\\s*=\\s*(?:0)*(\\d{1}|[1][0-9]|[2][0-3]):(?:0)*([0-9]|[0-5][0-9]):(?:0)*([0-5][0-9]|[0-9])(\n)*"), regex_constants::match_default)) {
			m_AllRooms.add(new CRoom(CString(string(sm[1]).c_str()), CTimeSpan(0, stoi(sm[2]), stoi(sm[3]), stoi(sm[4]))));
		}
		lineIndex++;
	}
	fclose(pFile);



	m_ListCtrl.SetExtendedStyle(m_ListCtrl.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EDITLABELS);

	CRect rect;
	m_ListCtrl.GetClientRect(&rect);
	int nColInterval = rect.Width() / 3;


	m_ListCtrl.InsertColumn(0, _T("Room Name"), LVCFMT_LEFT, nColInterval);
	m_ListCtrl.InsertColumn(1, _T("Time Duration"), LVCFMT_LEFT, nColInterval);
	m_ListCtrl.InsertColumn(2, _T("Time Left"), LVCFMT_LEFT, nColInterval);
	
	RefreshList();
	SetTimer(0, 1000, NULL);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CACManagerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	CDialogEx::OnSysCommand(nID, lParam);
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CACManagerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window
HCURSOR CACManagerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CACManagerDlg::RefreshList() {
	m_ListCtrl.DeleteAllItems();

	for (vector<CRoom*>::reverse_iterator it = m_AllRooms.m_pRooms.rbegin(); it != m_AllRooms.m_pRooms.rend(); it++) {
		int nIndex = m_ListCtrl.InsertItem(0, (*it)->m_szName);

		CString timeDuration = (*it)->m_TimeDuration.Format("%H:%M:%S");
		m_ListCtrl.SetItemText(nIndex, 1, timeDuration);

		timeDuration = "";
		timeDuration = (*it)->m_TimeLeftNow.Format("%H:%M:%S");
		m_ListCtrl.SetItemText(nIndex, 2, timeDuration);
		
	}
}


void CACManagerDlg::OnLvnItemchangedList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}


void CACManagerDlg::OnNMDblclkList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here
	int row = pNMItemActivate->iItem;
	int column = pNMItemActivate->iSubItem;
	if (column == 0) {
		CRoom* room = m_AllRooms.m_pRooms[row];
		if (room != nullptr)
			room->setActive(!room->m_isActive);
		RefreshList();
		Invalidate(false);
	}
	else if (column == 1) {
		CEditRoomDlg m_EditRoomDlg;
		m_EditRoomDlg.m_roomName = m_AllRooms.m_pRooms[row]->m_szName;
		CTimeSpan rdtime = m_AllRooms.m_pRooms[row]->m_TimeDuration;
		m_EditRoomDlg.m_cdtime = COleDateTime(2500, 2, 2, rdtime.GetHours(), rdtime.GetMinutes(), rdtime.GetSeconds());
		m_EditRoomDlg.DoModal();
	}
	*pResult = 0;
}


void CACManagerDlg::OnNMCustomdrawList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLVCUSTOMDRAW lpLVCustomDraw = reinterpret_cast<LPNMLVCUSTOMDRAW>(pNMHDR);

	switch (lpLVCustomDraw->nmcd.dwDrawStage)
	{
	case CDDS_ITEMPREPAINT:
	case CDDS_ITEMPREPAINT | CDDS_SUBITEM:
		if (lpLVCustomDraw->iSubItem == 0) {
			if (m_AllRooms.m_pRooms[lpLVCustomDraw->nmcd.dwItemSpec]->m_isActive)
				lpLVCustomDraw->clrTextBk = RGB(0, 255, 0);
			else
				lpLVCustomDraw->clrTextBk = RGB(255, 0, 0);
		}
		else
			lpLVCustomDraw->clrTextBk = RGB(255, 255, 255);

		break;

	default: break;
	}

	*pResult = 0;
	*pResult |= CDRF_NOTIFYPOSTPAINT;
	*pResult |= CDRF_NOTIFYITEMDRAW;
	*pResult |= CDRF_NOTIFYSUBITEMDRAW;
}


void CACManagerDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	for (vector<CRoom*>::iterator it = m_AllRooms.m_pRooms.begin(); it != m_AllRooms.m_pRooms.end(); it++) {
		if ((*it)->m_isActive) {
			if ((*it)->m_TimeLeftNow == CTimeSpan(0, 0, 0, 0)) {
				(*it)->setActive(false);
				continue;
			}
			(*it)->m_TimeLeftNow -= CTimeSpan(0, 0, 0, 1);
		}
	}
	RefreshList();
	CDialogEx::OnTimer(nIDEvent);
}

struct messageObj {
	CTimeSpan durationTime;
	CString roomName;
};


LRESULT CACManagerDlg::OnReceiveEdit(WPARAM wparam, LPARAM lparam) {
	
	messageObj* msgObj = reinterpret_cast<messageObj*>(wparam);

	CRoom* room = m_AllRooms.getRoomByName(msgObj->roomName);
	room->m_TimeDuration = msgObj->durationTime;
	if (room->m_isActive)
		room->m_TimeLeftNow = room->m_TimeDuration;

	return TRUE;
}

void CEditRoomDlg::OnBnClickedOk()
{
	//CString time;
	CTime durationDateTime;
	m_TimeDurationCtrl.GetTime(durationDateTime);
	messageObj msgObj = {
		CTimeSpan(0, durationDateTime.GetHour(), durationDateTime.GetMinute(), durationDateTime.GetSecond()),
		m_roomName
	};
	messageObj* pmsgObj = &msgObj;
	::SendMessage(*GetParent(), RECEIVE_EDIT, (WPARAM)pmsgObj, 0);

	CDialogEx::OnOK();
}


void CEditRoomDlg::OnDtnDatetimechangeDatetimepicker1(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMDATETIMECHANGE pDTChange = reinterpret_cast<LPNMDATETIMECHANGE>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}
